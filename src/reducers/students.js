import * as types from '../constants/ActionTypes';

var s4 = () => {
    return  Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

var randomID = () => {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

var findIndex = (students, id) => {
    var result = -1;
    students.forEach((student, index) => {
        if(student.id === id){
            result = index;
        }
    });
    return result;
}

var data = JSON.parse(localStorage.getItem('students'));
var initialState = data ? data : [];
var myReducer = (state = initialState, action) =>{
    var id = '';
    var index = -1;
    switch(action.type){
        case types.LIST_ALL:
            return state;
        case types.SAVE_STUDENT:
            var student = {
                id : action.student.id,
                name : action.student.name,
                status : (action.student.status === 'true' || action.student.status === true) ? true : false
            };
            if(!student.id){
                student.id = randomID();
                state.push(student);
            }else{
                index = findIndex(state, student.id);
                state[index] = student;
            }
            localStorage.setItem('students', JSON.stringify(state));
            return [...state];
        case types.UPDATE_STATUS_STUDENT:
            id = action.id;
            index = findIndex(state, id);
            state[index] = {
                ...state[index],
                status : !state[index].status
            };
            localStorage.setItem('students', JSON.stringify(state));
            return [...state];
        case types.DELETE_STUDENT:
            id = action.id;
            index = findIndex(state, id);
            state.splice(index, 1);
            localStorage.setItem('students', JSON.stringify(state));
            return [...state];
        default:
            return state;
    }
};

export default myReducer;