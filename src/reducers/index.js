import { combineReducers } from 'redux';
import students from './students';
import isDisplayForm from './isDisplayForm';
import itemEditing from './itemEditing';
import filterTable from './filterTable';
import search from './search';
import sort from './sort';

const myReducer = combineReducers({
    students, // students : students,
    isDisplayForm, // isDisplayForm : isDisplayForm
    itemEditing, // itemEditing : itemEditing
    filterTable,
    search,
    sort // sort : sort
});

export default myReducer;