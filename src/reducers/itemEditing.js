import * as types from './../constants/ActionTypes';

var initialState = {
    id : '',
    name : '',
    status : false
};
var myReducer = (state = initialState, action) =>{
    switch(action.type){
        case types.EDIT_STUDENT:
            return action.student;
        default:
            return state;
    }
};

export default myReducer;