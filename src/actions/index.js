import * as types from './../constants/ActionTypes';

export const listAll = () => {
    return {
        type : types.LIST_ALL
    }
};

export const saveStudent = (student) => {
    return {
        type : types.SAVE_STUDENT,
        student // 
    }
};

export const toggleForm = () => {
    return {
        type : types.TOGGLE_FORM
    }
}

export const openForm = () => {
    return {
        type : types.OPEN_FORM
    }
}

export const closeForm = () => {
    return {
        type : types.CLOSE_FORM
    }
}

export const updateStatus = (id) => {
    return {
        type : types.UPDATE_STATUS_STUDENT,
        id // id : id
    }
}

export const deleteStudent = (id) => {
    return {
        type : types.DELETE_STUDENT,
        id // id : id
    }
}

export const editStudent = (student) => {
    return {
        type : types.EDIT_STUDENT,
        student //
    }
}

export const filterStudent = (filter) => {
    return {
        type : types.FILTER_TABLE,
        filter // filter : filter -> filterName, filterStatus
    }
}

export const searchStudent = (keyword) => {
    return {
        type : types.SEARCH,
        keyword // keyword : keyword
    }
}

export const sortStudent = (sort) => {
    return {
        type : types.SORT,
        sort // sort : sort -> sort.by sort.value
    }
}