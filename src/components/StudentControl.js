import React, { Component } from 'react';
import StudentSearchControl from './StudentSearchControl';
import StudentSortControl from './StudentSortControl';

class StudentControl extends Component {
    render() {
        return (
            <div className="row mt-15">
                <StudentSearchControl />
                <StudentSortControl />
            </div>
        );
    }
}

export default StudentControl;
