import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/index';

class StudentItem extends Component {

    showStatusElement(){
        return (
            <span
                className={ this.props.student.status ? 'label label-danger' : 'label label-info' }
                onClick={ this.onUpdateStatus }
            >{ this.props.student.status === true ? 'Kích Hoạt' : 'Ẩn' }</span>
        );
    }

    onUpdateStatus = () => {
        this.props.onUpdateStatus(this.props.student.id);
    }

    onDeleteItem = () => {
        this.props.onDeleteStudent(this.props.student.id);
        this.props.onCloseForm();
    }

    onEditStudent = () => {
        this.props.onOpenForm();
        this.props.onEditStudent(this.props.student);
    }

    render() {
        return (
            <tr>
                <td>{ this.props.index }</td>
                <td>{ this.props.student.name }</td>
                <td className="text-center">
                    { this.showStatusElement() }
                </td>
                <td className="text-center">
                    <button
                        type="button"
                        className="btn btn-warning"
                        onClick={ this.onEditStudent }>
                        <span className="fa fa-pencil mr-5"></span>Sửa
                    </button>
                    &nbsp;
                    <button
                        type="button" className="btn btn-danger"
                        onClick={ this.onDeleteItem }>
                        <span className="fa fa-trash mr-5"></span>Xóa
                    </button>
                </td>
            </tr>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        onUpdateStatus : (id) => {
            dispatch(actions.updateStatus(id));
        },
        onDeleteStudent : (id) => {
            dispatch(actions.deleteStudent(id))
        },
        onCloseForm : () => {
            dispatch(actions.closeForm());
        },
        onOpenForm : () => {
            dispatch(actions.openForm());
        },
        onEditStudent : (student) => {
            dispatch(actions.editStudent(student));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StudentItem);
